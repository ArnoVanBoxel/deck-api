<?php

use App\Controllers\DeckController;
use App\Routing\Request;
use App\Routing\Response;
use App\Routing\Router;
use Dotenv\Dotenv;

include_once "vendor/autoload.php";

/**
 * Dotenv Loader
 */

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

/**
 * Routing
 */

$router = Router::getInstance();

// Deck routes

$deckController = new DeckController();

$router->route(Request::REQUEST_METHOD_POST, '/decks(/)', function (Request $request, Response $response) use ($deckController) {
    $deckController->store($request, $response);
});

$router->route(Request::REQUEST_METHOD_GET, '/decks/{id}(/)', function(Request $request, Response $response) use ($deckController) {
    $deckController->get($request, $response);
});

$router->route(Request::REQUEST_METHOD_PUT, '/decks/{id}/shuffle(/)', function(Request $request, Response $response) use ($deckController) {
    $deckController->shuffle($request, $response);
});

$router->route(Request::REQUEST_METHOD_DELETE, '/decks/{id}(/)', function(Request $request, Response $response) use ($deckController) {
    $deckController->delete($request, $response);
});

// Default route

$router->default(function() {
    echo "Hello world!";
});