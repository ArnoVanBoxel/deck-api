<?php

use App\Helpers\PDOCreator;

class PDOCreatorTest extends BaseTest
{
    public function testGetDsn()
    {
        $getDSN = $this->getNonPublicMethod(PDOCreator::class, 'getDSN');

        $this->assertEquals('mysql:dbname=testDatabase;host=testHost', $getDSN->invoke(new PDOCreator));
    }

    public function testGetDatabasePassword()
    {
        $getDatabasePassword = $this->getNonPublicMethod(PDOCreator::class, 'getDatabasePassword');

        $this->assertEquals('testPass', $getDatabasePassword->invoke(new PDOCreator));
    }

    public function testGetDatabaseUser()
    {
        $getDatabaseUser = $this->getNonPublicMethod(PDOCreator::class, 'getDatabaseUser');

        $this->assertEquals('testUser', $getDatabaseUser->invoke(new PDOCreator));
    }
}
