<?php

use App\Helpers\Randomiser;

class RandomiserTest extends BaseTest
{
    public function testRandomiser()
    {
        $randomiser = Randomiser::getInstance();
        $randomiser->setSeed(123);
        $this->assertEquals(28, $randomiser->random(0, 100));
    }
}