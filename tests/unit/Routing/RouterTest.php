<?php

use App\Exceptions\InvalidRouteException;
use App\Routing\Request;
use App\Routing\Router;

class RouterTest extends BaseTest
{
    private $router;

    public function setUp(): void
    {
        parent::setUp();

        $this->router = Router::getInstance();
    }

    public function testThrowsInvalidRouteException()
    {
        $this->expectException(InvalidRouteException::class);
        $this->expectExceptionMessage('Invalid Route: @some?invalid-route');

        $this->router->route(
            Request::REQUEST_METHOD_GET,
            '@some?invalid-route'
            , function () {
                echo "Hello world";
            }
        );
    }

    public function testGetParamsForMatch()
    {
        $getParams = $this->getNonPublicMethod(Router::class, 'getParams');

        $this->assertEquals(['someParam' => 'someValue', 0 => '/testingRoute/someValue', 1 => 'someValue'], $getParams->invokeArgs($this->router, ['/testingRoute/someValue', 'testingRoute/{someParam}']));
    }

    public function testGetParamsForNoMatch()
    {
        $getParams = $this->getNonPublicMethod(Router::class, 'getParams');

        $this->assertEquals([], $getParams->invokeArgs($this->router, ['/testingRoute/someValue', 'testingRoute']));
    }

    /**
     * @dataProvider regexProvider
     */
    public function testGetRouteRegex($route, $expectedRegex)
    {
        $getRouteRegex = $this->getNonPublicMethod(Router::class, 'getRouteRegex');

        $this->assertEquals($expectedRegex, $getRouteRegex->invokeArgs($this->router, [$route]));
    }

    public function regexProvider()
    {
        return [
            ['(/)', '@^/?$@D'],
            ['/login', '@^/login$@D'],
            ['/deck/{id}', '@^/deck/(?<id>[a-zA-Z0-9\_\-]+)$@D'],
            ['/deck/{deck_id}/card/{card_id}', '@^/deck/(?<deck_id>[a-zA-Z0-9\_\-]+)/card/(?<card_id>[a-zA-Z0-9\_\-]+)$@D'],
            ['/card/{card_id}(/)', '@^/card/(?<card_id>[a-zA-Z0-9\_\-]+)/?$@D']
        ];
    }

    /**
     * @dataProvider paramsProvider
     */
    public function testGetFilteredParams($params, $expectedFilteredParams)
    {
        $filterParams = $this->getNonPublicMethod(Router::class, 'filterParams');

        $this->assertEquals($expectedFilteredParams, $filterParams->invokeArgs($this->router, [$params]));
    }

    public function paramsProvider()
    {
        return [
            [
                ['someParam' => 'someValue', 0 => 'someValue', 1 => 'someParam'],
                ['someParam' => 'someValue']
            ],
            [
                ['deck_id' => '123', 0 => '123', 1 => '/'],
                ['deck_id' => '123']
            ],
        ];
    }
}