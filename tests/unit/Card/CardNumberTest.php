<?php

use App\Exceptions\CardNumberInvalidException;
use App\Models\Card\CardNumber;
use PHPUnit\Framework\TestCase;

class CardNumberTest extends TestCase
{
    public function testCardTooBigException()
    {
        $this->expectException(CardNumberInvalidException::class);
        $this->expectExceptionMessage('Card number is invalid : 14');

        new CardNumber(14);
    }

    public function testCardTooSmallException()
    {
        $this->expectException(CardNumberInvalidException::class);
        $this->expectExceptionMessage('Card number is invalid : -1');

        new CardNumber(-1);
    }
}