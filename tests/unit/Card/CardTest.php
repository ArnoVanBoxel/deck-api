<?php

use App\Models\Card\Card;
use App\Models\Card\CardNumber;
use App\Models\Card\Suit;
use PHPUnit\Framework\TestCase;

class CardTest extends TestCase
{
    public function testNewCard()
    {
        $card = new Card(new CardNumber(10), new Suit(Suit::SUIT_HEARTS));

        $this->assertEquals(new Suit(Suit::SUIT_HEARTS), $card->suit);
        $this->assertEquals(new CardNumber(10), $card->number);
    }
}