<?php

use App\Exceptions\SuitInvalidException;
use App\Models\Card\Suit;
use PHPUnit\Framework\TestCase;

class SuitTest extends TestCase
{
    public function testInvalidSuitThrowsException()
    {
        $this->expectException(SuitInvalidException::class);
        $this->expectExceptionMessage('Suit is invalid : 5');

        new Suit(5);
    }
}