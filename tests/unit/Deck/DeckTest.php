<?php

use App\Helpers\Randomiser;
use App\Models\Card\CardNumber;
use App\Models\Card\Suit;
use App\Models\Deck\Deck;
use App\Models\Card\Card;
use PHPUnit\Framework\TestCase;

class DeckTest extends TestCase
{
    public function testNewDeckWithJokers()
    {
        $deck = new Deck(true);

        $this->assertCount(54, $deck->getCardsLeft());
        $this->assertEquals(new Card(new CardNumber(13), new Suit(Suit::SUIT_HEARTS)), $deck->getCardsLeft()[25]);
        $this->assertEquals(new Card(new CardNumber(CardNumber::NUMBER_JOKER), null), $deck->getCardsLeft()[52]);
    }

    public function testShuffle()
    {
        $randomiser = Randomiser::getInstance();
        $randomiser->setSeed(123);

        $deck = new Deck();
        $deck->shuffle();

        $this->assertEquals(new Card(new CardNumber(11), new Suit(Suit::SUIT_DIAMONDS)), $deck->getCardsLeft()[3]);
    }

    public function testDrawCard()
    {
        $deck = new Deck();

        $this->assertEquals(new Card(new CardNumber(1), new Suit(Suit::SUIT_DIAMONDS)), $deck->draw());
        $this->assertCount(51, $deck->getCardsLeft());
        $this->assertCount(1, $deck->getCardsDrawn());
    }
}