<?php

namespace App\Helpers;

use App\Traits\Singleton;

/**
 * Class Randomiser
 * @package App\Helpers
 *
 * Used to randomise a class, can be seeded, only one instance exists.
 */
class Randomiser
{
    use Singleton;

    private int|null $seed = null;

    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    public function random(int $min, int $max): int
    {
        @mt_srand($this->seed);

        return @mt_rand($min, $max);
    }

    /**
     * @param int $seed
     */
    public function setSeed(int $seed): void
    {
        $this->seed = $seed;
    }
}