<?php

namespace App\Helpers;

/**
 * Class PDOCreator
 * @package App\Helpers
 */
class PDOCreator
{
    /**
     * @return \PDO
     */
    public static function getPDO(): \PDO
    {
        return new \PDO(self::getDSN(), self::getDatabaseUser(), self::getDatabasePassword());
    }

    /**
     * @return string
     */
    private static function getDSN() : string
    {
        return 'mysql:dbname=' . $_ENV['DBNAME'] . ';host=' . $_ENV['DBHOST'];
    }

    /**
     * @return string
     */
    private static function getDatabaseUser() : string
    {
        return $_ENV['DBUSER'];
    }

    /**
     * @return string
     */
    private static function getDatabasePassword() : string
    {
        return $_ENV['DBPASS'];
    }
}