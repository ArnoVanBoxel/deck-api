<?php

namespace App\Exceptions;

use Throwable;

/**
 * Class CardTooBigException
 * @package App\Exceptions
 */
class CardNumberInvalidException extends \Exception
{
    public function __construct(int $cardNumber, $code = 0, Throwable $previous = null)
    {
        parent::__construct('Card number is invalid : ' . $cardNumber, $code, $previous);
    }
}