<?php

namespace App\Exceptions;

use Throwable;

/**
 * Class SuitInvalidException
 * @package App\Exceptions
 */
class SuitInvalidException extends \Exception
{
    public function __construct(int $suit, $code = 0, Throwable $previous = null)
    {
        parent::__construct('Suit is invalid : ' . $suit, $code, $previous);
    }
}