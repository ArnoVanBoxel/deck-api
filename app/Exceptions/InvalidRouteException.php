<?php

namespace App\Exceptions;

use Throwable;

/**
 * Class InvalidRouteException
 * @package App\Exceptions
 */
class InvalidRouteException extends \Exception
{
    public function __construct($route, $code = 0, Throwable $previous = null)
    {
        parent::__construct('Invalid Route: ' . $route, $code, $previous);
    }
}