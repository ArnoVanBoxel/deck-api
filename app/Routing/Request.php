<?php

namespace App\Routing;

/**
 * Class Request
 * @package App\Routing
 *
 * Represents a request which can be handled by the callback of the router.
 */
class Request
{
    public $params;
    public $request_method;
    public $content_type;

    const CONTENT_TYPE_JSON = 'application/json';
    const PHP_INPUT = "php://input";

    // Request methods
    const REQUEST_METHOD_GET = 'get';
    const REQUEST_METHOD_POST = 'post';
    const REQUEST_METHOD_PUT = 'put';
    const REQUEST_METHOD_DELETE = 'delete';

    /**
     * Request constructor.
     * @param array $params
     * @param string $method
     * @param string $content_type
     */
    public function __construct(array $params, string $method, string $content_type)
    {
        $this->params = $params;
        $this->request_method = $method;
        $this->content_type = $content_type;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        if (strtolower($this->request_method) !== self::REQUEST_METHOD_POST) {
            return [];
        }

        $body = [];
        foreach ($_POST as $key => $value) {
            $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        return $body;
    }

    /**
     * @return array
     */
    public function getJSON(): array
    {
        if (strtolower($this->request_method) !== self::REQUEST_METHOD_POST) {
            return [];
        }

        if (strcasecmp($this->content_type, self::CONTENT_TYPE_JSON) !== 0) {
            return [];
        }

        // Receive the RAW post data.
        $content = trim(file_get_contents(self::PHP_INPUT));
        $decoded = json_decode($content, true);

        return $decoded;
    }

    /**
     * @param string $param
     * @return mixed|null
     */
    public function getJSONParam(string $param): mixed
    {
        return $this->getJSON()[$param] ?? null;
    }
}