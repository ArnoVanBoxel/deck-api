<?php

namespace App\Routing;

/**
 * Class Response
 * @package App\Routing
 */
class Response
{
    private $status = 200;

    // Response codes
    const CODE_SUCCESSFUL = 200;
    const CODE_BAD_REQUEST = 400;
    const CODE_INTERNAL_ERROR = 500;

    // Api responses
    const API_RESPONSE_OK = 'ok';
    const API_RESPONSE_FAIL = 'fail';

    /**
     * @param int $code
     * @return Response
     */
    public function status(int $code): Response
    {
        $this->status = $code;

        return $this;
    }

    /**
     * @param array $data
     */
    public function toJSON(array $data = []): void
    {
        http_response_code($this->status);
        header('Content-Type: ' . Request::CONTENT_TYPE_JSON);
        echo json_encode($data);
    }
}