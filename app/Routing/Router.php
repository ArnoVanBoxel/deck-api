<?php

namespace App\Routing;

use App\Exceptions\InvalidRouteException;
use App\Traits\Singleton;

class Router
{
    use Singleton;

    private $routed = false;

    /**
     * @param string $route
     * @param $callback
     *
     * @throws InvalidRouteException
     */
    public function route(string $method, string $route, $callback): void
    {
        if ($this->routed) {
            return;
        }

        if (preg_match('/[^-:\/_{}()a-zA-Z\d]/', $route)) {
            throw new InvalidRouteException($route);
        }

        if (strtolower($_SERVER['REQUEST_METHOD']) !== $method) {
            return;
        }

        $requestUriBeforeQuery = strpos($_SERVER['REQUEST_URI'], '?') !== false ? substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) : $_SERVER['REQUEST_URI'];
        $params = $this->getParams($requestUriBeforeQuery, $route);
        if (!$params) {
            return;
        }

        $params = $this->filterParams($params);
        $params = $params + $_GET;

        $this->callback($callback, $params);
    }

    /**
     * Fallback route, if route was not specified user will be routed here
     *
     * @param $callback
     */
    public function default($callback): void
    {
        if (!$this->routed) {
            $callback(new Request(
                [],
                trim($_SERVER['REQUEST_METHOD']),
                !empty($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : ''
            ), new Response());
        }
    }

    /**
     * Used to check whether the route is the expected route,
     * in which case it will return the params.
     *
     * @param string $route
     * @param string $expectedRoute
     * @return bool
     */
    private function getParams(string $route, string $expectedRoute): array
    {
        $expectedRoute = (stripos($expectedRoute, '/') !== 0) ? '/' . $expectedRoute : $expectedRoute;

        $checkRoute = preg_match($this->getRouteRegex($expectedRoute), $route, $params);

        return $checkRoute ? $params : [];
    }

    /**
     * Execute the callback.
     *
     * @param string $route
     * @param $callback (function)
     * @param array $params
     */
    private function callback($callback, array $params): void
    {
        $this->routed = true;

        $callback(new Request(
            $params,
            trim($_SERVER['REQUEST_METHOD']),
            !empty($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : ''
        ), new Response());
    }

    /**
     * Get the regex for checking the route and receiving the params.
     *
     * @param string $route
     * @return string
     */
    private function getRouteRegex(string $route): string
    {
        $route = preg_replace('#\(/\)#', '/?', $route);
        $route = preg_replace(
            '/{([a-zA-Z0-9_\-]+)}/',    # Replace "{parameter}"
            '(?<$1>[a-zA-Z0-9\_\-]+)', # with "(?<parameter>[a-zA-Z0-9\_\-]+)"
            $route
        );

        return "@^" . $route . "$@D";
    }

    /**
     * Filter the parameters to only return the ones we actually want.
     *
     * @param array $params
     * @return array
     */
    private function filterParams(array $params): array
    {
        return array_intersect_key(
            $params,
            array_flip(array_filter(array_keys($params), 'is_string'))
        );
    }
}