<?php

namespace App\Traits;

trait Singleton
{
    protected static $instance = null;

    protected function __construct() {}

    public static function getInstance(): self {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __clone() {}
}