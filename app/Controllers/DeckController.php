<?php

namespace App\Controllers;

use App\Models\Card\Suit;
use App\Models\Deck\Deck;
use App\Routing\Request;
use App\Routing\Response;
use phpDocumentor\Reflection\Types\Void_;

/**
 * Class DeckController
 * @package App\Controllers
 */
class DeckController
{
    public function store(Request $request, Response $response): void
    {
        $include_jokers = $request->getJSONParam('include_jokers') ?? false;

        $deck = new Deck($include_jokers);
        if (!$deck->save()) {
            $response->status(Response::CODE_INTERNAL_ERROR)->toJSON([
                'status' => Response::API_RESPONSE_FAIL
            ]);

            return;
        }

        $this->successfulResponse($response, $deck);
    }

    public function get(Request $request, Response $response): void
    {
        $id = $request->params['id'] ?? null;
        if (!$id) {
            $response->status(Response::CODE_BAD_REQUEST)->toJSON([
                'error' => 'Missing \'id\' parameter.',
                'status' => Response::API_RESPONSE_FAIL
            ]);

            return;
        }

        $deck = Deck::find($id);
        if (!$deck) {
            $response->status(Response::CODE_BAD_REQUEST)->toJSON([
                'error' => 'Deck not found.',
                'status' => Response::API_RESPONSE_FAIL
            ]);

            return;
        }

        $this->successfulResponse($response, $deck);
    }

    public function shuffle(Request $request, Response $response): void
    {
        $id = $request->params['id'] ?? null;
        if (!$id) {
            $response->status(Response::CODE_BAD_REQUEST)->toJSON([
                'error' => 'Missing \'id\' parameter.',
                'status' => Response::API_RESPONSE_FAIL
            ]);

            return;
        }

        $deck = Deck::find($id);
        if (!$deck) {
            $response->status(Response::CODE_BAD_REQUEST)->toJSON([
                'error' => 'Deck not found.',
                'status' => Response::API_RESPONSE_FAIL
            ]);

            return;
        }

        $deck->shuffle();
        if (!$deck->save()) {
            $response->status(Response::CODE_INTERNAL_ERROR)->toJSON([
                'status' => Response::API_RESPONSE_FAIL
            ]);
        }

        $this->successfulResponse($response, $deck);
    }

    public function delete(Request $request, Response $response): void
    {
        $id = $request->params['id'] ?? null;
        if (!$id) {
            $response->status(Response::CODE_BAD_REQUEST)->toJSON([
                'error' => 'Missing \'id\' parameter.',
                'status' => Response::API_RESPONSE_FAIL
            ]);

            return;
        }

        $deck = Deck::find($id);
        if (!$deck) {
            $response->status(Response::CODE_BAD_REQUEST)->toJSON([
                'error' => 'Deck not found.',
                'status' => Response::API_RESPONSE_FAIL
            ]);

            return;
        }

        $deck->delete();

        $response->status(Response::CODE_SUCCESSFUL)->toJSON([
            'status' => Response::API_RESPONSE_OK
        ]);
    }

    private function successfulResponse(Response $response, Deck $deck): void
    {
        $response->status(Response::CODE_SUCCESSFUL)->toJSON([
            'deck' => [
                'id' => $deck->id,
                'include_jokers' => $deck->include_jokers,
                'cards_left' => $deck->getCardsLeft(),
                'cards_drawn' => $deck->getCardsDrawn(),
            ],
            'status' => Response::API_RESPONSE_OK
        ]);
    }
}