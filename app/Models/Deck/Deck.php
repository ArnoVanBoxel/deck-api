<?php

namespace App\Models\Deck;

use App\Helpers\PDOCreator;
use App\Helpers\Randomiser;
use App\Models\Card\Card;
use App\Models\Card\CardNumber;
use App\Models\Card\Suit;
use App\Models\DatabaseModel;

/**
 * Class Deck
 * @package App\Deck
 *
 * @property int $id
 * @property bool $include_jokers
 * @property array $cards_left
 * @property array $cards_drawn
 */
class Deck extends DatabaseModel
{
    public array $cards_drawn = [];

    /**
     * Deck constructor.
     * @param bool $include_jokers
     */
    public function __construct(
        public bool $include_jokers = false,
        public array $cards_left = [],
    ) {
        if (!$cards_left) $this->addDefaultCards();
    }

    /**
     * Default cards that will be in a deck,
     * if different cards in a deck are provided this will not be called
     */
    public function addDefaultCards(): void
    {
        foreach (array_keys(Suit::$suits) as $suit) {
            for ($i = CardNumber::NUMBER_ACE; $i <= CardNumber::MAX_NUMBER; $i++) {
                $this->cards_left[] = new Card(new CardNumber($i), new Suit($suit));
            }
        }
        if ($this->include_jokers) $this->cards_left[] = new Card(new CardNumber(CardNumber::NUMBER_JOKER), null);
        if ($this->include_jokers) $this->cards_left[] = new Card(new CardNumber(CardNumber::NUMBER_JOKER), null);
    }

    /**
     * Shuffle the deck in a random order
     */
    public function shuffle(): void
    {
        $randomiser = Randomiser::getInstance();

        for ($i = 0; $i < count($this->cards_left); $i++)
        {
            $j = $randomiser->random(0, $i);
            $temp = $this->cards_left[$i];
            $this->cards_left[$i] = $this->cards_left[$j];
            $this->cards_left[$j] = $temp;
        }
    }

    /**
     * Draws a card by returning the first card in the deck and then removing it from the deck.
     *
     * @return Card
     */
    public function draw(): Card
    {
        $this->cards_drawn[] = $card = $this->cards_left[0];

        array_shift($this->cards_left);

        return $card;
    }

    /**
     * @return array
     */
    public function getCardsLeft(): array
    {
        return $this->cards_left;
    }

    /**
     * @return array
     */
    public function getCardsDrawn(): array
    {
        return $this->cards_drawn;
    }

    /**
     * Find a single Deck based on ID
     *
     * @param int $id
     * @return $this|null
     */
    public static function find(int $id): ?self
    {
        $pdo = PDOCreator::getPDO();
        $q = $pdo->prepare('SELECT * FROM decks WHERE id = :id');
        $q->bindValue(':id', $id);

        if (!$q->execute()) {
            return null;
        }

        $deckArray = $q->fetch();
        if (empty($deckArray)) {
            return null;
        }

        $deck = new Deck($deckArray['include_jokers'] ?? false);
        $deck->id = $deckArray['id'];
        $deck->cards_drawn = @unserialize($deckArray['cards_drawn']);
        $deck->cards_left = @unserialize($deckArray['cards_left']);

        return $deck;
    }

    /**
     * @return bool
     */
    protected function update(): bool
    {
        $pdo = PDOCreator::getPDO();
        $q = $pdo->prepare('UPDATE decks SET include_jokers = :include_jokers, cards_left = :cards_left, cards_drawn = :cards_drawn WHERE id = :id');
        $q->bindValue(':id', $this->id);
        $q->bindValue(':include_jokers', (int) $this->include_jokers);
        $q->bindValue(':cards_left', $this->getSerializedCardsLeft());
        $q->bindValue(':cards_drawn', $this->getSerializedCardsDrawn());

        return $q->execute();
    }

    /**
     * @return bool
     */
    protected function insert(): bool
    {
        $pdo = PDOCreator::getPDO();
        $q = $pdo->prepare('INSERT INTO decks (include_jokers, cards_left, cards_drawn) VALUES (:include_jokers, :cards_left, :cards_drawn)');
        $q->bindValue(':include_jokers', (int) $this->include_jokers);
        $q->bindValue(':cards_left', $this->getSerializedCardsLeft());
        $q->bindValue(':cards_drawn', $this->getSerializedCardsDrawn());

        $executed = $q->execute();

        if (!$executed) return false;

        $q = $pdo->query('SELECT MAX(id) AS id FROM decks');
        $id = $q->fetch();

        $this->id = $id['id'] ?? null;

        return true;
    }

    /**
     * @return string
     */
    private function getSerializedCardsLeft(): string
    {
        return @serialize($this->getCardsLeft());
    }

    /**
     * @return string
     */
    private function getSerializedCardsDrawn(): string
    {
        return @serialize($this->cards_drawn);
    }

    /**
     * Delete the record.
     */
    public function delete(): bool
    {
        $pdo = PDOCreator::getPDO();
        $q = $pdo->prepare('DELETE FROM decks WHERE id = :id');
        $q->bindValue(':id', $this->id);

        return $q->execute();
    }
}