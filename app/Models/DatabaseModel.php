<?php

namespace App\Models;

use App\Helpers\PDOCreator;

/**
 * Class DatabaseModel
 * @package App\Models
 */
abstract class DatabaseModel
{
    public function save(): bool
    {
        return empty($this->id) ? $this->insert() : $this->update();
    }

    abstract protected function update(): bool;
    abstract protected function insert(): bool;
}