<?php

namespace App\Models\Card;

use App\Exceptions\CardNumberInvalidException;

/**
 * Class CardNumber
 * @package App\Models\Card
 *
 * @property int $number
 */
class CardNumber
{
    // Non-numeric representation of cards
    private static $nonNumeric = [
        0 => 'Joker',
        1 => 'A',
        11 => 'J',
        12 => 'Q',
        13 => 'K',
    ];

    // Min and max numbers
    const MIN_NUMBER = 0;
    const MAX_NUMBER = 13;

    // Specific card numbers
    const NUMBER_JOKER = 0;
    const NUMBER_ACE = 1;

    /**
     * CardNumber constructor.
     *
     * @param int $number
     */
    public function __construct(
        public int $number
    ) {
        if ($number < self::MIN_NUMBER || $number > self::MAX_NUMBER) throw new CardNumberInvalidException($number);
    }
}