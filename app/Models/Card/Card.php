<?php

namespace App\Models\Card;

/**
 * Class Card
 * @package App\Models\Card
 *
 * @property int $number
 * @property Suit $suit
 */
class Card
{
    /**
     * Card constructor.
     * @param int $number
     * @param Suit $suit
     */
    public function __construct(
        public CardNumber $number,
        public Suit|null $suit
    ) {}
}