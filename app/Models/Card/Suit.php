<?php

namespace App\Models\Card;

use App\Exceptions\SuitInvalidException;

/**
 * Class Suit
 * @package App\Models\Card
 *
 * @property int $suit
 */
class Suit
{
    const SUIT_DIAMONDS = 1;
    const SUIT_HEARTS = 2;
    const SUIT_SPADES = 3;
    const SUIT_CLUBS = 4;

    /**
     * @var array
     */
    public static $suits = [
        self::SUIT_DIAMONDS => 'Diamonds',
        self::SUIT_HEARTS => 'Hearts',
        self::SUIT_SPADES => 'Spades',
        self::SUIT_CLUBS => 'Clubs',
    ];

    /**
     * Suit constructor.
     * @param int $suit
     */
    public function __construct(
       public int $suit
    ) {
        if (!in_array($suit, array_keys(self::$suits))) throw new SuitInvalidException($suit);
    }
}